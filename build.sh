#/bin/sh
mvn clean install
mkdir target/hebdobot
cp target/hebdobot-*.jar target/hebdobot/hebdobot.jar
cp exe/* target/hebdobot
cp LICENSE target/hebdobot
mvn dependency:copy-dependencies -DoutputDirectory=target/hebdobot/lib
launch4j launch4j.xml
(cd target; zip -r9 hebdobot.zip hebdobot)
