package fr.imirhil.april.hebdobot.irc;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import fr.imirhil.april.hebdobot.review.Review;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "/fr/imirhil/april/hebdobot/conf.xml")
public class BotTest implements ReviewListener {
	private static class BotMock extends Bot {
		public BotMock() throws Exception {
			super("", 0, "bot", "channel");
		}

		@Override
		public void onMessage(final String channel, final String sender,
				final String login, final String hostname, final String message) {
			super.onMessage(channel, sender, login, hostname, message);
		}
	}

	@Test
	public void redo() throws Exception {
		final Bot bot = new BotMock();
		bot.add(this);

		final InputStream is = BotTest.class
				.getResourceAsStream("/fr/imirhil/april/hebdobot/review.log");
		if (is == null) {
			return;
		}
		try {
			final Reader isr = new InputStreamReader(is);
			try {
				final BufferedReader isbr = new BufferedReader(isr);
				try {
					String line;
					final Pattern pattern = Pattern
							.compile(".*\\s+<([^>]+)>\\s+(.*)");
					while ((line = isbr.readLine()) != null) {
						final Matcher matcher = pattern.matcher(line);
						if (matcher.matches()) {
							bot.onMessage("channel", matcher.group(1), "", "",
									matcher.group(2));
						}
					}
				} finally {
					isbr.close();
				}

			} finally {
				isr.close();
			}
		} finally {
			is.close();
		}
	}

	@Override
	public void onEnd(final Review review) {
		try {
			final String date = ISODateTimeFormat.basicDate().print(
					new DateTime());
			final String text = review.toString();
			final File file = new File("target/" + date + "_revue.txt");
			FileUtils.writeStringToFile(file, text);
		} catch (final IOException e) {
			throw new RuntimeException(e);
		}
	}
}
