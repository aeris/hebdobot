package fr.imirhil.april.hebdobot.irc;

import fr.imirhil.april.hebdobot.review.Review;

public interface ReviewListener {
	void onEnd(Review review);
}
