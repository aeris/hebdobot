package fr.imirhil.april.hebdobot;

import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

@Component
public final class Context implements ApplicationContextAware {
	private static ApplicationContext CONTEXT;

	@Override
	public void setApplicationContext(
			final ApplicationContext applicationContext) throws BeansException {
		CONTEXT = applicationContext;
	}

	public static <T> T getBean(final Class<T> clazz) {
		return CONTEXT.getBean(clazz);
	}

	public static void close() {
		if (CONTEXT instanceof ConfigurableApplicationContext) {
			((ConfigurableApplicationContext)CONTEXT).close();
		}
	}

	@SuppressWarnings("unchecked")
	public static <T> T getBean(final String name) {
		return (T) CONTEXT.getBean(name);
	}

}
