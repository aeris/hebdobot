package fr.imirhil.april.hebdobot.pastebin;

import java.util.LinkedList;
import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.protocol.HTTP;

public class PastebinClient {
	private static final String API_DEV_KEY = "api_dev_key";
	private static final String API_USER_KEY = "api_user_key";
	private static final String API_USER_NAME = "api_user_name";
	private static final String API_USER_PASSWORD = "api_user_password";
	private static final String API_OPTION = "api_option";
	private static final String API_PASTE_PRIVATE = "api_paste_private";
	private static final String API_PASTE_NAME = "api_paste_name";
	private static final String API_PASTE_EXPIRATION = "api_paste_expire_date";
	private static final String API_PASTE_FORMAT = "api_paste_format";
	private static final String API_PASTE_CODE = "api_paste_code";

	private static final String API_ERROR = "Bad API request,";

	public static class APIException extends Exception {
		private static final long serialVersionUID = 1L;

		private APIException(final String message) {
			super(message);
		}

		private static void throwIfError(final String content)
				throws APIException {
			if (content.contains(API_ERROR)) {
				throw new APIException(content.replaceFirst(API_ERROR, ""));
			}
		}
	}

	private final String apiKey;
	private String apiUserKey = null;
	HttpClient httpClient = new DefaultHttpClient();

	public PastebinClient(final String apiKey) {
		this.apiKey = apiKey;
	}

	public void login(final String name, final String password)
			throws Exception {
		final List<NameValuePair> params = new LinkedList<NameValuePair>();
		setParameter(params, API_DEV_KEY, this.apiKey);
		setParameter(params, API_USER_NAME, name);
		setParameter(params, API_USER_PASSWORD, password);

		final HttpPost request =
				new HttpPost("http://pastebin.com/api/api_login.php");
		request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));

		final HttpResponse response = this.httpClient.execute(request);
		final String content =
				IOUtils.toString(response.getEntity().getContent());
		APIException.throwIfError(content);
		this.apiUserKey = content;
	}

	public String paste(final String code) throws Exception {
		return this.paste(code, null, Format.NONE, Private.PUBLIC,
				Expiration.DAY_1);
	}

	public String paste(final String code, final Private privat)
			throws Exception {
		return this.paste(code, null, Format.NONE, privat, Expiration.DAY_1);
	}

	public String paste(final String code, final String name) throws Exception {
		return this.paste(code, name, Format.NONE, Private.PUBLIC,
				Expiration.DAY_1);
	}

	public String paste(final String code, final String name,
			final Private privat) throws Exception {
		return this.paste(code, name, Format.NONE, privat, Expiration.DAY_1);
	}

	public String paste(final String code, final String name,
			final Format format) throws Exception {
		return this.paste(code, name, format, Private.PUBLIC, Expiration.DAY_1);
	}

	public String paste(final String code, final String name,
			final Format format, final Private privat) throws Exception {
		return this.paste(code, name, format, privat, Expiration.DAY_1);
	}

	public String paste(final String code, final Format format)
			throws Exception {
		return this.paste(code, null, format, Private.PUBLIC, Expiration.DAY_1);
	}

	public String paste(final String code, final Format format,
			final Private privat) throws Exception {
		return this.paste(code, null, format, privat, Expiration.DAY_1);
	}

	public String paste(final String code, final Expiration expiration)
			throws Exception {
		return this.paste(code, null, Format.NONE, Private.PUBLIC, expiration);
	}

	public String paste(final String code, final Private privat,
			final Expiration expiration) throws Exception {
		return this.paste(code, null, Format.NONE, privat, expiration);
	}

	public String paste(final String code, final Format format,
			final Expiration expiration) throws Exception {
		return this.paste(code, null, format, Private.PUBLIC, expiration);
	}

	public String paste(final String code, final Format format,
			final Private privat, final Expiration expiration) throws Exception {
		return this.paste(code, null, format, privat, expiration);
	}

	public String paste(final String code, final String name,
			final Format format, final Expiration expiration) throws Exception {
		return this.paste(code, name, format, Private.PUBLIC, expiration);
	}

	public String paste(final String code, final String name,
			final Format format, final Private privat,
			final Expiration expiration) throws Exception {
		final List<NameValuePair> params = new LinkedList<NameValuePair>();
		setParameter(params, API_DEV_KEY, this.apiKey);
		setParameter(params, API_USER_KEY, this.apiUserKey);
		setParameter(params, API_OPTION, Option.PASTE.getValue());
		setParameter(params, API_PASTE_PRIVATE, privat.getValue());
		setParameter(params, API_PASTE_NAME, name);
		setParameter(params, API_PASTE_EXPIRATION, expiration.getValue());
		setParameter(params, API_PASTE_FORMAT, format.getValue());
		setParameter(params, API_PASTE_CODE, code);

		final HttpPost request =
				new HttpPost("http://pastebin.com/api/api_post.php");
		request.setEntity(new UrlEncodedFormEntity(params, HTTP.UTF_8));

		final HttpResponse response = this.httpClient.execute(request);
		final String content =
				IOUtils.toString(response.getEntity().getContent());
		APIException.throwIfError(content);
		return content;
	}

	private static void setParameter(final List<NameValuePair> params,
			final String name, final String value) {
		if (value == null) {
			return;
		}
		params.add(new BasicNameValuePair(name, value));
	}
}
