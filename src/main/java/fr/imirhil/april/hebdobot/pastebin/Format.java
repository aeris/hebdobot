package fr.imirhil.april.hebdobot.pastebin;

public enum Format {
	NONE(null), C("c"), CPP("cpp"), JAVA("java"), JAVA_5("java5");

	private final String value;

	private Format(final String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
