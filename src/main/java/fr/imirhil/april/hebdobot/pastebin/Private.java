package fr.imirhil.april.hebdobot.pastebin;

public enum Private {
	PUBLIC("0"), UNLISTED("1"), PRIVATE("2");

	private String value;

	private Private(final String value) {
		this.value = value;
	}

	String getValue() {
		return this.value;
	}
}
