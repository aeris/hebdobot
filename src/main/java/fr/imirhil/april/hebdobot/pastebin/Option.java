package fr.imirhil.april.hebdobot.pastebin;

public enum Option {
	PASTE("paste");

	private final String value;

	private Option(final String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
