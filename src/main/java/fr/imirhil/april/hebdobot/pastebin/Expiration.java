package fr.imirhil.april.hebdobot.pastebin;

public enum Expiration {
	NEVER(null), MINUTE_10("10M"), HOUR_1("1H"), DAY_1("1D"), MONTH_1("1M");

	private final String value;

	private Expiration(final String value) {
		this.value = value;
	}

	public String getValue() {
		return this.value;
	}
}
