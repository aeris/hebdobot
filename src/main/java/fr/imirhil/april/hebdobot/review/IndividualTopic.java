package fr.imirhil.april.hebdobot.review;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class IndividualTopic extends Topic {
	private final Map<String, List<Message>> messages =
			new HashMap<String, List<Message>>();

	public IndividualTopic(final String title) {
		super(title);
	}

	@Override
	public void add(final Message message) {
		final String author = message.getAuthor();
		if (!this.messages.containsKey(author)) {
			this.messages.put(author, new LinkedList<Message>());
		}
		this.messages.get(author).add(message);
	}

	@Override
	public Set<String> getParticipants() {
		return this.messages.keySet();
	}

	public List<Message> getMessages(final String author) {
		return this.messages.get(author);
	}

	public boolean hasParticipant(final String participant) {
		return this.messages.containsKey(participant);
	}
}
