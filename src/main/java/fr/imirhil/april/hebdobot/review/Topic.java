package fr.imirhil.april.hebdobot.review;

import java.util.Set;

public abstract class Topic {
	private final String title;

	protected Topic(final String title) {
		this.title = title;
	}

	public String getTitle() {
		return this.title;
	}

	public abstract void add(Message message);

	public abstract Set<String> getParticipants();
}
