package fr.imirhil.april.hebdobot.review;

import org.joda.time.DateTime;

public class Message {
	private final DateTime date = new DateTime();
	private final String author;
	private final String content;

	public Message(final String author, final String content) {
		this.author = author;
		this.content = content;
	}

	public DateTime getDate() {
		return this.date;
	}

	public String getAuthor() {
		return this.author;
	}

	public String getContent() {
		return this.content;
	}
}
