package fr.imirhil.april.hebdobot.review;

import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang.StringUtils;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;

import fr.imirhil.april.hebdobot.Context;
import fr.imirhil.april.hebdobot.xml.UserAlias;

public class Review {
	private static final int LENGTH = 80;

	private static final UserAlias USER_ALIAS = Context
			.getBean(UserAlias.class);
	private final Set<String> participants = new HashSet<String>();
	private final List<IndividualTopic> individualTopics =
			new LinkedList<IndividualTopic>();
	private final List<CollectiveTopic> collectiveTopics =
			new LinkedList<CollectiveTopic>();
	private Topic currentTopic;
	private final List<Message> messages = new LinkedList<Message>();
	private final String owner;

	public String getOwner() {
		return this.owner;
	}

	public boolean isOwner(final String name) {
		return this.owner.equalsIgnoreCase(name);
	}

	public Review(final String owner) {
		this.owner = owner;
	}

	public Topic getCurrentTopic() {
		return this.currentTopic;
	}

	public void begin(final IndividualTopic topic) {
		this.individualTopics.add(topic);
		this.currentTopic = topic;
	}

	public void begin(final CollectiveTopic topic) {
		this.collectiveTopics.add(topic);
		this.currentTopic = topic;
	}

	public void add(final Message message) {
		if (this.currentTopic != null) {
			this.participants.add(message.getAuthor());
			this.currentTopic.add(message);
		}
		this.addRaw(message);
	}

	public void addRaw(final Message message) {
		this.messages.add(message);
	}

	@Override
	public String toString() {
		final StringBuffer buffer = new StringBuffer();
		addLine(buffer, '=');
		addCenter(buffer, "Revue de la semaine en cours");
		addEmpty(buffer);
		addCenter(buffer, DateTimeFormat.fullDate().print(new DateTime()));
		addLine(buffer, '=');
		addEmpty(buffer);
		addEmpty(buffer);

		addLine(buffer, '=');
		addEmpty(buffer);
		addCenter(buffer, "Participants", '-');
		for (final String participant : this.participants) {
			addChunk(buffer, "* " + USER_ALIAS.getRealName(participant) + "\n");
		}

		if (!this.individualTopics.isEmpty()) {
			for (final String participant : this.participants) {
				addEmpty(buffer);
				addLine(buffer, '=');
				addEmpty(buffer);
				addCenter(buffer, USER_ALIAS.getRealName(participant), '-');
				for (final IndividualTopic topic : this.individualTopics) {
					if (topic.hasParticipant(participant)) {
						addEmpty(buffer);
						add(buffer, getTopic(topic));
						addEmpty(buffer);
						for (final Message message : topic
								.getMessages(participant)) {
							addChunk(buffer, "* " + message.getContent() + "\n");
						}
					}
				}
			}
		}

		if (!this.collectiveTopics.isEmpty()) {
			for (final CollectiveTopic topic : this.collectiveTopics) {
				addEmpty(buffer);
				addLine(buffer, '=');
				addCenter(buffer, topic.getTitle());
				addLine(buffer, '=');
				addEmpty(buffer);
				for (final Message message : topic.getMessages()) {
					addChunk(buffer, "* " + message.getAuthor() + " : "
							+ message.getContent() + "\n");
				}
			}
		}

		addEmpty(buffer);
		addCenter(buffer, "Log IRC brut");
		addEmpty(buffer);
		for (final Message message : this.messages) {
			addChunk(buffer,
					"* " + message.getAuthor() + " : " + message.getContent()
							+ "\n");
		}

		return buffer.toString();
	}

	private static String getLine(final Character c) {
		return StringUtils.repeat(c.toString(), LENGTH) + "\n";
	}

	private static String getLine(final String content, final char c) {
		return StringUtils.center(" " + content + " ", LENGTH, c) + "\n";
	}

	private static String getTopic(final Topic topic) {
		return "===  " + topic.getTitle() + "  ===\n";
	}

	private static void addCenter(final StringBuffer buffer,
			final String content, final char c) {
		buffer.append(getLine(content, c));
	}

	private static void addCenter(final StringBuffer buffer,
			final String content) {
		addCenter(buffer, content, ' ');
	}

	private static void addLine(final StringBuffer buffer, final char c) {
		buffer.append(getLine(c));
	}

	private static void addEmpty(final StringBuffer buffer) {
		buffer.append("\n");
	}

	private static String chunk(final String content, final int length) {
		final String[] words = content.split(" ");
		final StringBuffer result = new StringBuffer();
		StringBuffer current = new StringBuffer();

		for (final String word : words) {
			if (current.length() + word.length() > length) {
				if (result.length() > 0) {
					result.append("\n");
				}
				result.append(current);
				current = new StringBuffer(word);
			} else {
				if (current.length() > 0) {
					current.append(" ");
				}
				current.append(word);
			}
		}

		if (current.length() > 0) {
			if (result.length() > 0) {
				result.append("\n");
			}
			result.append(current);
		}

		return result.toString();
	}

	private static void add(final StringBuffer buffer, final String content) {
		buffer.append(content);
	}

	private static String chunk(final String content) {
		return chunk(content, LENGTH);
	}

	private static void
			addChunk(final StringBuffer buffer, final String content) {
		add(buffer, chunk(content));
	}

	public Collection<String> getParticipants() {
		final Collection<String> reviewers = new HashSet<String>();
		for (final Topic topic : this.individualTopics) {
			reviewers.addAll(topic.getParticipants());
		}
		for (final Topic topic : this.collectiveTopics) {
			reviewers.addAll(topic.getParticipants());
		}
		return reviewers;
	}

}
