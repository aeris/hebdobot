package fr.imirhil.april.hebdobot.review;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;

public class CollectiveTopic extends Topic {
	private final List<Message> messages = new LinkedList<Message>();

	public CollectiveTopic(final String title) {
		super(title);
	}

	@Override
	public void add(final Message message) {
		this.messages.add(message);
	}

	@Override
	public Set<String> getParticipants() {
		final Set<String> participants = new HashSet<String>();
		for (final Message message : this.messages) {
			participants.add(message.getAuthor());
		}
		return participants;
	}

	public List<Message> getMessages() {
		return this.messages;
	}
}
