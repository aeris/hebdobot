package fr.imirhil.april.hebdobot.xml;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.SAXException;

public class UserAlias {
	private final Map<String, String> aliases = new HashMap<String, String>();

	public UserAlias(final InputStream source) {
		try {
			final Unmarshaller u =
					JAXBContext.newInstance(Users.class).createUnmarshaller();
			u.setSchema(SchemaFactory
					.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI)
					.newSchema(
							UserAlias.class
									.getResource("/fr/imirhil/april/hebdobot/users.xsd")));

			for (final User user : u
					.unmarshal(new StreamSource(source), Users.class)
					.getValue().getUser()) {
				final String realName = user.getRealName();
				for (final String nick : user.getNick()) {
					this.aliases.put(nick, realName);
				}
			}
		} catch (final SAXException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		} catch (final JAXBException e) {
			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	public String getRealName(final String nick) {
		for (final Entry<String, String> entry : this.aliases.entrySet()) {
			if (nick.toLowerCase().contains(entry.getKey().toLowerCase())) {
				return entry.getValue() + " ( " + nick + " )";
			}
		}
		return nick;
	}
}
