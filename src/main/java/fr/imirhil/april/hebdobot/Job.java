package fr.imirhil.april.hebdobot;

import javax.annotation.Resource;

import org.springframework.social.twitter.api.impl.TwitterTemplate;

import fr.imirhil.april.hebdobot.irc.Bot;

public class Job {
	@Resource
	private Bot bot;
	@Resource
	private TwitterTemplate twitterClient;
	private String tweet;
	private String irc;

	public void setTweet(final String message) {
		this.tweet = message;
	}

	public void setIrc(final String message) {
		this.irc = message;
	}

	private void notify(final int min) {
		this.bot.sendMessage(String.format(this.irc, min));
		final String tweet = String.format(this.tweet, min);
		this.twitterClient.timelineOperations().updateStatus(tweet);
	}

	public void at30() {
		this.notify(30);
	}

	public void at45() {
		this.notify(15);
	}

	public void at55() {
		this.notify(5);
	}
}
