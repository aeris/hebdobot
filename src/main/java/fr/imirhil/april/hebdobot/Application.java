package fr.imirhil.april.hebdobot;

import java.io.File;
import java.util.Properties;

import org.apache.commons.io.FileUtils;
import org.joda.time.DateTime;
import org.joda.time.format.ISODateTimeFormat;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.support.FileSystemXmlApplicationContext;

import fr.imirhil.april.hebdobot.irc.Bot;
import fr.imirhil.april.hebdobot.irc.ReviewListener;
import fr.imirhil.april.hebdobot.pastebin.PastebinClient;
import fr.imirhil.april.hebdobot.pastebin.Private;
import fr.imirhil.april.hebdobot.review.Review;

public class Application implements ReviewListener {
	private static final Logger LOGGER = LoggerFactory
			.getLogger(Application.class);

	private static final String FILE_SUFFIX = "file.suffix";

	private final Properties properties = Context.getBean("properties");
	private final Bot bot = Context.getBean(Bot.class);
	private final PastebinClient pastebinClient = Context
			.getBean(PastebinClient.class);

	private Application() throws Exception {
		this.bot.add(this);
	}

	@Override
	public void onEnd(final Review review) {
		final String date = ISODateTimeFormat.basicDate().print(new DateTime());
		final String text = review.toString();
		try {
			this.bot.sendMessage("% Compte-rendu de la revue : "
					+ this.pastebinClient.paste(text, "Revue APRIL " + date,
							Private.UNLISTED));
		} catch (final Exception e) {
			LOGGER.error("Error during Pastebin submit", e);
		}

		if (this.properties.containsKey(FILE_SUFFIX)) {
			try {
				final File file =
						new File(date + "_"
								+ this.properties.getProperty(FILE_SUFFIX));
				FileUtils.writeStringToFile(file, text);
				this.bot.sendMessage("% Compte-rendu de la revue : "
						+ file.getName());
			} catch (final Exception e) {
				LOGGER.error("Error during file generation", e);
			}
		}
	}

	public static void main(final String[] args) throws Exception {
		new FileSystemXmlApplicationContext(System.getProperty("spring.conf",
				"conf.xml")).registerShutdownHook();
		new Application();
	}
}
