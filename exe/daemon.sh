#!/bin/sh

NAME=hebdobot
BASE_DIR=$HOME/$NAME
PID_FILE=$BASE_DIR/$NAME.pid
DAEMON=java
DAEMON_ARGS="-cp *:lib/* fr.imirhil.april.hebdobot.Application"

CMD="/sbin/start-stop-daemon --chdir $BASE_DIR --quiet --make-pidfile --pidfile $PID_FILE --exec $DAEMON"
case "$1" in
	start)
		echo "Starting $NAME"
		$CMD --start --test || exit 1
		$CMD --start --background -- $DAEMON_ARGS || exit 2
		echo "$NAME started"
		;;
	stop)
		echo "Stopping $NAME"
		$CMD --stop || exit 3
		rm -f $PID_FILE
		echo "$NAME stopped"
		;;
esac
